class Tide {

  static get(url: string, success, error) {
    let request = new XMLHttpRequest();
    request.open('GET', url, true);

    request.onload = function () {
      if (this.status >= 200 && this.status < 400) {
        let data = JSON.parse(this.response);
        success(data);
      } else {
        error(request);
      }
    };

    request.onerror = function () {
      error();
    };

    request.send();
  }

  static post(url: string, data, success, error) {
    let request = new XMLHttpRequest();
    request.open('POST', url, true);
    request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    request.onreadystatechange = () => {
      if(request.readyState == 4) {
        if (request.status >= 200 && request.status < 400) {
          let data = JSON.parse(request.responseText);
          success(data);
        }
        else {
          error(request);
        }
      }
    };
    request.send(JSON.stringify(data));
  }

}